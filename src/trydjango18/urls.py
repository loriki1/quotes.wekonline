from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = [
    
    url(r'^$', 'newsletter.views.home', name='home'),
    url(r'^contact/$', 'newsletter.views.contact', name='contact'),
    url(r'^login/$', 'newsletter.views.login', name='login'),
    url(r'^signup/$', 'newsletter.views.signup', name='signup'),
    url(r'^about/$', 'newsletter.views.about', name='about'),
    url(r'^home/$', 'newsletter.views.home', name='home'),
    url(r'^admin/', include(admin.site.urls)),
    
] 

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
	urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
